package test.layout;

import java.awt.Font;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import com.teamdev.jxbrowser.browser.Browser;
import com.teamdev.jxbrowser.engine.Engine;
import com.teamdev.jxbrowser.engine.EngineOptions;
import com.teamdev.jxbrowser.engine.RenderingMode;
import com.teamdev.jxbrowser.view.swing.BrowserView;

import test.CourseVO;
import test.DAO;
import test.UserVO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

public class Not_User_Main_GUI {

	private JFrame frame2;
	private JPanel jp;
	Browser browser;
	BrowserView view;
	private JTable table;

	public Not_User_Main_GUI() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		frame2 = new JFrame();
		frame2.setBounds(50, 50, 600, 950);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.getContentPane().setLayout(null);
		
		// login/out img
		ImageIcon ic_in = new ImageIcon("data/img/gui/enter.png");

		// 크기 변경
		Image img_in = ic_in.getImage();
		Image img_in2 = img_in.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		ImageIcon ic_in2 = new ImageIcon(img_in2);
		
		JLabel lbl_img_login = new JLabel(ic_in2);
		lbl_img_login.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frame2.dispose();
				Login_GUI l = new Login_GUI();
				l.main(null);
			}
		});
		lbl_img_login.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_img_login.setBounds(530, 10, 30, 30);
		frame2.getContentPane().add(lbl_img_login);
		
		
		JLabel lbl_welcome = new JLabel("로그인이 필요합니다.");
		lbl_welcome.setFont(new Font("Monospaced", Font.PLAIN, 15));
		lbl_welcome.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_welcome.setBounds(280, 10, 250, 30);
		frame2.getContentPane().add(lbl_welcome);
		
		DAO dao = new DAO();
		
		//Title
		JLabel title = new JLabel("\uC81C\uC8FC \uC804\uB3D9 \uD0A5\uBCF4\uB4DC \uB9AC\uC2A4\uD2B8");
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(new Font("굴림", Font.BOLD, 30));
		title.setSize(560, 50);
		title.setLocation(10, 50);
		frame2.getContentPane().add(title);
		
		// 지도 설정
		jp = new JPanel();
		jp.setSize(560, 400);
		jp.setLocation(10, 100);
		frame2.getContentPane().add(jp);
		
		jp.setLayout(new BoxLayout(jp,BoxLayout.Y_AXIS));
		
		System.setProperty("jxbrowser.chromium.dir", "lib/jxbrowser");
		
		Engine engine = Engine.newInstance(
		        EngineOptions.newBuilder(RenderingMode.HARDWARE_ACCELERATED)
		                .licenseKey("1BNDHFSC1FXY3Y12XRR3V0TWSS1W8ZSEP0M4BIS4VTDGW1X34LEJ08M6VI9FII01E6WQ0P")
		                .build());
		
		browser = engine.newBrowser();
		browser.navigation().loadUrl(new File("data/mapView2.html").getAbsolutePath());
		
		SwingUtilities.invokeLater(() -> {
			  // Create the Swing BrowserView component
			  view = BrowserView.newInstance(browser);

			  frame2.addWindowListener(new WindowAdapter() {
				  @Override
				  public void windowClosing(WindowEvent e) {
					  engine.close();
				  }
			  });
			  frame2.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			  jp.add(view);

			});
		
		// TBL
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setSize(560, 300);
		scrollPane.setLocation(10, 540);
		frame2.getContentPane().add(scrollPane);
		
		dao.insert_course();
		
		ArrayList<CourseVO> list = dao.courseListShort();
		
		// 임의 테이블 값 생성
		String[] col = {"코스번호", "코스이름", "난이도", "재미", "컨디션"};
		Object[][] data = new Object[list.size()][col.length];
		
		for(int i = 0; i < list.size(); i++) {
			data[i][0] = list.get(i).getCourse_no();
			data[i][1] = list.get(i).getCourse_name();
			data[i][2] = list.get(i).getRating_diff();
			data[i][3] = list.get(i).getRating_fun();
			data[i][4] = list.get(i).getRating_cond();
		}

		// 실 TBL에 값 넣어줄 DefaultTableModel 생성 + 수정 불가
		DefaultTableModel model = new DefaultTableModel(data, col) {
			public boolean isCellEditable(int rowIndex, int mColindex) {
				return false;
			}
		};
		
		table = new JTable(model);
		table.setFillsViewportHeight(true);
		table.setSurrendersFocusOnKeystroke(true);
		table.setFont(new Font("monospaced", Font.PLAIN, 17));
		scrollPane.setViewportView(table);

		JComboBox comboBox = new JComboBox(col);
		comboBox.setSize(100, 30);
		comboBox.setLocation(10, 505);
		frame2.getContentPane().add(comboBox);
		
		JTextPane txt_search = new JTextPane();
		txt_search.setToolTipText("");
		txt_search.setFont(new Font("monospaced", Font.PLAIN, 14));
		txt_search.setSize(150, 30);
		txt_search.setLocation(115, 505);
		frame2.getContentPane().add(txt_search);
		
		// 검색 버튼
		JButton btn_search = new JButton("검색");
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String field = (String)comboBox.getSelectedItem();
				String search = txt_search.getText();
				
				if(search.equals("")) {
					JOptionPane.showMessageDialog(txt_search, "검색어가 없습니다.\n검색창을 확인해주세요.");
				}else {
					
					for(int i = 0; i<table.getRowCount(); i++) {
						if(!search.equals(table.getValueAt(i, 0)) && !search.equals(table.getValueAt(i, 1))) {
							DefaultTableModel model3 = (DefaultTableModel)table.getModel();
							model3.removeRow(i);
						}
					}
				}
			}
		});
		btn_search.setSize(80, 30);
		btn_search.setLocation(490, 505);
		frame2.getContentPane().add(btn_search);
		
		JButton btn_detail = new JButton("조회");
		btn_detail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int rowIndex = (int) table.getValueAt(table.getSelectedRow(), 0)-1;
				System.out.println(rowIndex);
				
				if(rowIndex >= 0) {
//					DefaultTableModel model = (DefaultTableModel)table.getModel();
					Not_User_Course_GUI nc = new Not_User_Course_GUI(rowIndex);

				}else {
					JOptionPane.showMessageDialog(table, "Error. Check selection");
				}
				
			}
		});
		btn_detail.setSize(120, 30);
		btn_detail.setLocation(450, 850);
		frame2.getContentPane().add(btn_detail);
		
		JRadioButton rdbtn_diff = new JRadioButton("\uB09C\uC774\uB3C4");
		rdbtn_diff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		rdbtn_diff.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtn_diff.setBounds(275, 505, 70, 30);
		frame2.getContentPane().add(rdbtn_diff);
		
		JRadioButton rdbtn_fun = new JRadioButton("\uC7AC\uBBF8");
		rdbtn_fun.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtn_fun.setBounds(345, 505, 70, 30);
		frame2.getContentPane().add(rdbtn_fun);
		
		JRadioButton rdbtn_cond = new JRadioButton("\uCEE8\uB514\uC158");
		rdbtn_cond.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtn_cond.setBounds(415, 505, 70, 30);
		frame2.getContentPane().add(rdbtn_cond);

		jp.setVisible(true);
		
		frame2.setVisible(true);
	}
	


}
