package test.layout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import test.CourseVO;
import test.DAO;
import test.ReservationVO;
import test.UserVO;

import javax.swing.Icon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Mypage_GUI {

	private JFrame frame;
	private JTable table_reservation;
	private JTable table_review;
//
//	/**
//	 * Launch the application.
//	 */
//	public static void main(UserVO vo) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Mypage_GUI window = new Mypage_GUI(vo);
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public Mypage_GUI(UserVO vo) {
		initialize(vo);
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(UserVO vo) {
		DAO dao = new DAO();
		
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 950);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbl_welcome = new JLabel("<dynamic>\uB2D8, \uD658\uC601\uD569\uB2C8\uB2E4.");
		lbl_welcome.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_welcome.setFont(new Font("Monospaced", Font.PLAIN, 15));
		lbl_welcome.setBounds(292, 10, 250, 30);
		frame.getContentPane().add(lbl_welcome);
		
		JLabel lbl_img_logout = new JLabel((Icon) null);
		lbl_img_logout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		lbl_img_logout.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_img_logout.setBounds(542, 10, 30, 30);
		frame.getContentPane().add(lbl_img_logout);
		
		JButton btn_back = new JButton("\uB3CC\uC544\uAC00\uAE30");
		btn_back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				
				MapView_GUI mg = new MapView_GUI(vo);
			}
		});
		btn_back.setBounds(12, 15, 100, 30);
		frame.getContentPane().add(btn_back);
		
		JScrollPane scrollPane_reservation = new JScrollPane();
		scrollPane_reservation.setBounds(10, 101, 560, 300);
		frame.getContentPane().add(scrollPane_reservation);
		
		ArrayList<ReservationVO> list = dao.select_review(vo);
		
		// 임의 테이블 값 생성
		String[] col = {"예약 코스명", "예약일"};
		Object[][] data_res = new Object[list.size()][col.length];
		
		for(int i = 0; i < list.size(); i++) {
			data_res[i][0] = list.get(i).getCourse_name();
			data_res[i][1] = list.get(i).getDate();
		}

		// 실 TBL에 값 넣어줄 DefaultTableModel 생성 + 수정 불가
		DefaultTableModel model_reservation = new DefaultTableModel(data_res, col) {
			public boolean isCellEditable(int rowIndex, int mColindex) {
				return false;
			}
		};

		table_reservation = new JTable(model_reservation);
		table_reservation.setFillsViewportHeight(true);
		table_reservation.setSurrendersFocusOnKeystroke(true);
		table_reservation.setFont(new Font("monospaced", Font.PLAIN, 17));
		scrollPane_reservation.setViewportView(table_reservation);
		
		JLabel title_reservation = new JLabel("\uC608\uC57D \uB9AC\uC2A4\uD2B8");
		title_reservation.setHorizontalAlignment(SwingConstants.CENTER);
		title_reservation.setFont(new Font("굴림", Font.BOLD, 30));
		title_reservation.setBounds(10, 50, 560, 50);
		frame.getContentPane().add(title_reservation);
		
		JLabel title_review = new JLabel("\uC791\uC131 \uD6C4\uAE30 \uB9AC\uC2A4\uD2B8");
		title_review.setHorizontalAlignment(SwingConstants.CENTER);
		title_review.setFont(new Font("굴림", Font.BOLD, 30));
		title_review.setBounds(10, 435, 560, 50);
		frame.getContentPane().add(title_review);
		
		JScrollPane scrollPane_review = new JScrollPane();
		scrollPane_review.setBounds(10, 485, 560, 300);
		frame.getContentPane().add(scrollPane_review);
		
//		ArrayList<ReservationVO> list = dao.select_review(vo);
//		
//		// 임의 테이블 값 생성
//		String[] col = {"예약 코스명", "예약일"};
//		Object[][] data_res = new Object[list.size()][col.length];
//		
//		for(int i = 0; i < list.size(); i++) {
//			data_res[i][0] = list.get(i).getCourse_name();
//			data_res[i][1] = list.get(i).getDate();
//		}
//
//		// 실 TBL에 값 넣어줄 DefaultTableModel 생성 + 수정 불가
//		DefaultTableModel model_reservation = new DefaultTableModel(data_res, col) {
//			public boolean isCellEditable(int rowIndex, int mColindex) {
//				return false;
//			}
//		};
//
//		table_review = new JTable(model_reservation);
//		table_review.setFillsViewportHeight(true);
//		table_review.setSurrendersFocusOnKeystroke(true);
//		table_review.setFont(new Font("monospaced", Font.PLAIN, 17));
//		scrollPane_review.setViewportView(table_review);
		
		table_review = new JTable();
		scrollPane_review.setViewportView(table_review);
		
		JButton btn_review_regi = new JButton("\uD6C4\uAE30 \uB4F1\uB85D");
		btn_review_regi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Review r = new Review();
			}
		});
		btn_review_regi.setBounds(475, 800, 100, 30);
		frame.getContentPane().add(btn_review_regi);
	}
}
