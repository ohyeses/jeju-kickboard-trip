package test;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAO {
	
	private Connection conn = null;
	private PreparedStatement psmt = null;
	private ResultSet rs = null;
	
	private void getConnection() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String url = "jdbc:oracle:thin:@localhost:1521:xe";
			String user = "hr";
			String password = "hr";			
			conn = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void getClosed() {
		try {
			if(rs!=null) {
				rs.close();
			}
			if (psmt != null) {
				psmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//회원가입
	public int insert_userInfo(UserVO vo){
		int result=0;
		try {
			getConnection();
			String sql = "insert into user_info values(?,?,?,?)";
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getUser_id());
			psmt.setString(2, vo.getUser_pw());
			psmt.setString(3, vo.getUser_name());
			psmt.setString(4, vo.getUser_license());
			result = psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getClosed();
		}
		return result;	
	}
	
	public int check_userInfoID(UserVO vo){
		int result = 0;
		
		try {
			getConnection();
			String sql = "SELECT COUNT(*) FROM user_info WHERE user_id = ?";
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getUser_id());

			rs = psmt.executeQuery();
			
			if(rs.next()){
				result = rs.getInt("COUNT(*)");
//				int f = 1;
//				System.out.println("test >>>> "+result);
//				System.out.println("test2 >> "+f);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getClosed();
		}
		
		return result;	
	}
	
	public int check_userInfoLicense(UserVO vo){
		int result = 0;
		
		try {
			getConnection();
			String sql = "SELECT COUNT(*) FROM user_info WHERE user_license = ?";
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getUser_license());

			rs = psmt.executeQuery();
			
			if(rs.next()){
				result = rs.getInt("COUNT(*)");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getClosed();
		}
		
		return result;	
	}
	
	//Login >>기존 유저 정보가 아닌 예약정보를 합쳐서 보여줘야 할거
	// 로긴의 유저정보 받아오면 자동으로 받아온 유저정보 대입해 예약정보 알아서 받아오도록 하면 될듯
	//uservo가 아닌 다른 객체가 필요? ㄴㄴ
	public UserVO login(UserVO vo) {
		UserVO resultVO=null;
		
		try {
			getConnection();
			String sql = "SELECT * FROM user_info WHERE user_id = ? AND user_pw = ?";
			
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getUser_id());
			psmt.setString(2, vo.getUser_pw());
			
			rs = psmt.executeQuery();
			
			if(rs.next()){
				String id2=rs.getString("user_id");
				String pw2=rs.getString("user_pw");
				String name=rs.getString("user_name");
				String license = rs.getString("user_license");
				
				System.out.println(id2+ pw2+ name+ license);
				
				resultVO= new UserVO(id2, pw2, name, license);
			}	
				
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getClosed();
		}
		return resultVO;
	}
	
	//예약 추가
	public int insert_reservation(ReservationVO vo){
		int result=0;
		try {
			getConnection();
			String sql = "insert into reservation values(?,?,?)";
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getUser_id());
			psmt.setInt(2, vo.getCourse_no());
			psmt.setDate(3, vo.getDate());
			result = psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getClosed();
		}
		return result;	
	}
	
	//예약취소
	public int cancel_reservation(ReservationVO rvo) {
		int result=0;
		try {
			getConnection();
			String sql ="delete from reservation where user_id=? and course_no=? and date=?";
			psmt= conn.prepareStatement(sql);
			psmt.setString(1,rvo.getUser_id());
			psmt.setInt(2,rvo.getCourse_no());
			psmt.setDate(3,rvo.getDate());
			result = psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getClosed();
		}
		return result;
	}

	//예약목록확인
	public ArrayList<ReservationVO> show_reservation(ReservationVO rvo){
		ArrayList<ReservationVO> list= new ArrayList<ReservationVO>();
		try {
			getConnection();
			String sql="select * from reservation where user_id =? and course_no=? and date=?";
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, rvo.getUser_id());
			psmt.setInt(2, rvo.getCourse_no());
			psmt.setDate(3, rvo.getDate());
			rs=psmt.executeQuery();
			if(rs.next()) {
				String user_id=rs.getString("user_id");
				int course_no=rs.getInt("courese_no");
				Date date = rs.getDate("date");
				ReservationVO rvo1=new ReservationVO(user_id, course_no, date);
				list.add(rvo1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			getClosed();
		}
		return list;
	}
	
	
	//csv파일 읽어오는 method
	public static List<String[]> readFromCsvFile(String separator, String fileName){
	       try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
	           List<String[]> list = new ArrayList<>();
	           String line = "";
	           while((line = reader.readLine()) != null){
	               String[] array = line.split(separator);
	               list.add(array);
	           }
	           return list;
	       } catch (IOException e) {
	           e.printStackTrace();
	           return null;
	       }  
	   }

	//코스 디비에 넣기 
   public int insert_course(){
      String path = new File("data/data.csv").getAbsolutePath();
      String comm=",";
      String sql;
      List<String[]> list =readFromCsvFile(comm,path);
      int result=0;
      int checkResult =0;
      try {
         getConnection();

		for(int i = 1; i < list.size(); i++) {
			sql="select COUNT(*) from course_info where course_no = ? AND course_name = ? AND course_start = ? AND course_end = ?";
			
			psmt=conn.prepareStatement(sql);
			
			psmt.setInt(1, Integer.parseInt(list.get(i)[0]));
			psmt.setString(2, list.get(i)[1]);
			psmt.setString(3, list.get(i)[4]);
            psmt.setString(4, list.get(i)[5]);
			
			rs=psmt.executeQuery();
			
			if(rs.next()){
				checkResult = rs.getInt("COUNT(*)");
				if(checkResult == 0) {
					sql = "insert into course_info values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					psmt = conn.prepareStatement(sql);
					psmt.setInt(1, Integer.parseInt(list.get(i)[0]));
		            psmt.setString(2, list.get(i)[1]);
		            psmt.setString(3, list.get(i)[2]);
		            psmt.setString(4, list.get(i)[3]);
		            psmt.setString(5, list.get(i)[4]);
		            psmt.setString(6, list.get(i)[5]);
		            psmt.setString(7, list.get(i)[6]);
		            psmt.setString(8, list.get(i)[7]);
		            psmt.setInt(9, Integer.parseInt(list.get(i)[8]));
		            psmt.setInt(10, Integer.parseInt(list.get(i)[9]));
		            psmt.setInt(11, Integer.parseInt(list.get(i)[10]));
		            psmt.setString(12, list.get(i)[11]);
		            psmt.setString(13, list.get(i)[12]);
		            psmt.setString(14, list.get(i)[13]);
		            psmt.setString(15, list.get(i)[14]);
	               
		            result = psmt.executeUpdate();
		            }
				}
			}
		
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
         getClosed();
      }
      return result;   
   }
	
	
	//course정보 한 개 로우의 전체 확인하기
	public ArrayList<CourseVO> courseInfo(int no){
		ArrayList<CourseVO> list= new ArrayList<CourseVO>();
		try {
			getConnection();
			
			String sql="select * from course_info where course_no = ? ";
			
			psmt=conn.prepareStatement(sql);
			psmt.setInt(1,no);
			
			rs=psmt.executeQuery();
			
			if(rs.next()) {
				int course_no = rs.getInt("course_no");
				String course_name=rs.getString("course_name");
				String course_thm=rs.getString("course_thm");
				String course_intro=rs.getString("course_intro");
				String course_start=rs.getString("course_start");
				String course_end=rs.getString("course_end");
				String duration_time=rs.getString("duration_time");
				String course_len=rs.getString("course_len");
				int rating_diff=rs.getInt("rating_diff");
				int rating_fun=rs.getInt("rating_fun");
				int rating_cond=rs.getInt("rating_cond");
				String pic_path=rs.getString("pic_path");
				String other1=rs.getString("course_other1");
				String other2=rs.getString("course_other2");
				String other3=rs.getString("course_other3");
				CourseVO cvo1=new CourseVO(course_no,course_name,course_thm,course_intro,
						course_start,course_end,duration_time,course_len,rating_diff,rating_fun,
						rating_cond, pic_path,other1,other2,other3);
				list.add(cvo1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			getClosed();
		}
		return list;
	}
	
	//course정보 전체 row의 전체 확인하기
	public ArrayList<CourseVO> courseList(){
		ArrayList<CourseVO> list= new ArrayList<CourseVO>();
		try {
			getConnection();
			
			String sql="select * from course_info";
			
			psmt=conn.prepareStatement(sql);
			
			rs=psmt.executeQuery();
			
			while(rs.next()) {
				int course_no = rs.getInt("course_no");
				String course_name=rs.getString("course_name");
				String course_thm=rs.getString("course_thm");
				String course_intro=rs.getString("course_intro");
				String course_start=rs.getString("course_start");
				String course_end=rs.getString("course_end");
				String duration_time=rs.getString("duration_time");
				String course_len=rs.getString("course_len");
				int rating_diff=rs.getInt("rating_diff");
				int rating_fun=rs.getInt("rating_fun");
				int rating_cond=rs.getInt("rating_cond");
				String pic_path=rs.getString("pic_path");
				String other1=rs.getString("course_other1");
				String other2=rs.getString("course_other2");
				String other3=rs.getString("course_other3");
				CourseVO cvo1=new CourseVO(course_no,course_name,course_thm,course_intro,
						course_start,course_end,duration_time,course_len, rating_diff, rating_fun,
						rating_cond, pic_path,other1,other2,other3);
				list.add(cvo1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			getClosed();
		}
		return list;
	}
	
	// MapView하단 TBl의 간략 코스
	public ArrayList<CourseVO> courseListShort(){
		ArrayList<CourseVO> list= new ArrayList<CourseVO>();
		try {
			getConnection();
			
			String sql="select * from course_info";
			
			psmt=conn.prepareStatement(sql);
			
			rs=psmt.executeQuery();
			
			while(rs.next()) {
				int course_no = rs.getInt("course_no");
				String course_name=rs.getString("course_name");
				int rating_diff=rs.getInt("rating_diff");
				int rating_fun=rs.getInt("rating_fun");
				int rating_cond=rs.getInt("rating_cond");
				CourseVO cvo1=new CourseVO(course_no,course_name,rating_diff,rating_fun,rating_cond);
				list.add(cvo1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			getClosed();
		}
		return list;
	}
	

	//리뷰등록
	public int insert_review(ReviewVO vo){
		int result=0;
		try {
			getConnection();
			String sql = "insert into review values(?,?,?,?,?,?,?)";
			psmt = conn.prepareStatement(sql);
			psmt.setInt(1, vo.getReview_no());
			psmt.setInt(2, vo.getCourse_no());
			psmt.setString(3, vo.getUser_id());
			psmt.setInt(4, vo.getUrating_diff());
			psmt.setInt(5, vo.getUrating_fun());
			psmt.setInt(6, vo.getUrating_cond());
			psmt.setString(7, vo.getReview());
			result = psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			getClosed();
		}
		return result;	
	}
	
	//개인 예약 조회
	public ArrayList<ReservationVO> select_review(UserVO uvo){
		ArrayList<ReservationVO> list= new ArrayList<ReservationVO>();
		try {
			getConnection();
			
			String sql="select c.course_name, r.reservation_date from reservation r, user_info u, couser_info c "+
					"WHERE r.user_id = u.user_id AND r.courser_no = c.courser.no AND r.user_id = ?";
			
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, uvo.getUser_id());
			
			rs=psmt.executeQuery();
			
			while(rs.next()) {
				String course_name = rs.getString("course_name");
				Date date = rs.getDate("reservation_date");
				ReservationVO rvo1=new ReservationVO(course_name, date);
				list.add(rvo1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			getClosed();
		}
		return list;
	}

}
