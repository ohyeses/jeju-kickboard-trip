package test;

import java.sql.Date;

public class ReservationVO {
	private String user_id ;
	private int course_no;
	private String course_name;
	private Date date;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getCourse_no() {
		return course_no;
	}
	public void setCourse_no(int course_no) {
		this.course_no = course_no;
	}
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public ReservationVO(String user_id, int course_no, Date date) {
		this.user_id = user_id;
		this.course_no = course_no;
		this.date = date;
	}
	
	public ReservationVO(String course_name, Date date) {
		this.course_name = course_name;
		this.date = date;
	}
	
	
	
}