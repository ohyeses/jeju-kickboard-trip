package test.layout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import test.DAO;
import test.UserVO;

import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Login_GUI {

	private JFrame frame;
	private JTextField txt_id;
	private JPasswordField txt_pw;
	private String id="", pw="";
	private UserVO vo = null;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login_GUI window = new Login_GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login_GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbl_title = new JLabel("\uB85C\uADF8\uC778");
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_title.setBounds(10, 24, 410, 50);
		frame.getContentPane().add(lbl_title);
		
		JLabel lbl_id = new JLabel("ID");
		lbl_id.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_id.setBounds(20, 90, 95, 35);
		frame.getContentPane().add(lbl_id);
		
		JLabel lbl_pw = new JLabel("PASSWORD");
		lbl_pw.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_pw.setBounds(20, 140, 95, 35);
		frame.getContentPane().add(lbl_pw);
		
		txt_id = new JTextField();
		txt_id.setBounds(125, 90, 190, 30);
		frame.getContentPane().add(txt_id);
		txt_id.setColumns(20);
		
		txt_pw = new JPasswordField();
		txt_pw.setColumns(10);
		txt_pw.setBounds(125, 140, 190, 30);
		frame.getContentPane().add(txt_pw);
		
		JButton btnNewButton = new JButton("\uB85C\uADF8\uC778");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				id = txt_id.getText();
				pw = txt_pw.getText();
				
				if(id.length() != 0 && pw.length() != 0) {
					vo = new UserVO(id, pw);
					
					DAO dao = new DAO();
					UserVO result = dao.login(vo);
				
					if(result.getUser_id().length() != 0) {
						
						MapView_GUI m = new MapView_GUI(vo);
						frame.dispose();
						
					}else {
						JOptionPane.showMessageDialog(lbl_pw, "Login Fail");
					}
				}else {
					JOptionPane.showMessageDialog(lbl_pw, "공란이 있습니다.\n다시 확인해주세요");
				}
			}
		});
		btnNewButton.setBounds(340, 80, 80, 90);
		frame.getContentPane().add(btnNewButton);
		
		JButton btn_join = new JButton("\uD68C\uC6D0\uAC00\uC785");
		btn_join.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Join_GUI j = new Join_GUI();
				j.main(null);
				
				frame.dispose();
			}
		});
		btn_join.setBounds(280, 190, 140, 40);
		frame.getContentPane().add(btn_join);
		
		JButton btn_join_not = new JButton("\uBE44\uD68C\uC6D0 \uC9C4\uD589");
		btn_join_not.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Not_User_Main_GUI nm = new Not_User_Main_GUI();
			}
		});
		btn_join_not.setBounds(125, 190, 140, 40);
		frame.getContentPane().add(btn_join_not);
	}
}
