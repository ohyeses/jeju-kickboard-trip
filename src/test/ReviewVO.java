package test;

public class ReviewVO {
	private int review_no;
	private String  user_id;
	private int course_no;
	private int urating_diff;
	private int urating_fun;
	private int urating_cond;
	private String review;
	public ReviewVO(int review_no, String user_id, int course_no, int urating_diff, int urating_fun, int urating_cond,
			String review) {
		this.review_no = review_no;
		this.user_id = user_id;
		this.course_no = course_no;
		this.urating_diff = urating_diff;
		this.urating_fun = urating_fun;
		this.urating_cond = urating_cond;
		this.review = review;
	}
	public int getReview_no() {
		return review_no;
	}
	public void setReview_no(int review_no) {
		this.review_no = review_no;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getCourse_no() {
		return course_no;
	}
	public void setCourse_no(int course_no) {
		this.course_no = course_no;
	}
	public int getUrating_diff() {
		return urating_diff;
	}
	public void setUrating_diff(int urating_diff) {
		this.urating_diff = urating_diff;
	}
	public int getUrating_fun() {
		return urating_fun;
	}
	public void setUrating_fun(int urating_fun) {
		this.urating_fun = urating_fun;
	}
	public int getUrating_cond() {
		return urating_cond;
	}
	public void setUrating_cond(int urating_cond) {
		this.urating_cond = urating_cond;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
}