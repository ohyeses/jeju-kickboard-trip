package test.layout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class LoadingGUI {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoadingGUI window = new LoadingGUI();
					window.frame.setVisible(true);
					try {
						Not_User_Main_GUI m = new Not_User_Main_GUI();
						Thread.sleep(3000);
						window.frame.dispose();
						
					} catch (Exception e) {
						System.out.println(e.getMessage());
						// TODO: handle exception
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoadingGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(50, 50, 600, 950);
		frame.getContentPane().setLayout(null);
		
		JLabel lbl_title = new JLabel("Loading...");
		lbl_title.setFont(new Font("Monospaced", Font.BOLD | Font.ITALIC, 30));
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_title.setBounds(160, 500, 300, 50);
		frame.getContentPane().add(lbl_title);
		
		
	}

}
