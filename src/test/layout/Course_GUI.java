package test.layout;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import test.CourseVO;
import test.DAO;
import test.UserVO;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JPanel;
import javax.swing.ScrollPaneConstants;
import javax.swing.JProgressBar;

public class Course_GUI {

	private JFrame frame;

	private JLabel lblCourse;
	private JLabel lblCourseThm;
	private JLabel lblCourseIntro;
	private JLabel lblDurTime;
	private JLabel lblCourseLen;

	private JLabel lblother1;
	private JLabel lblother2;
	private JLabel lblother3;
	
	private JButton btnreservation;
	private JButton btncourse;
	
	private JScrollPane scrollPane_info;
	
	private JScrollPane scrollPane_img;
	private JLabel lbl_etc_img1;
	private JLabel lbl_etc_img2;
	private JLabel lbl_etc_img3;
	
	private JLabel lblDiff;
	private JLabel lblFun;
	private JLabel lblCond;
	
	private JLabel lbldiff_img_1,lbldiff_img_2, lbldiff_img_3, lbldiff_img_4, lbldiff_img_5;
	private JLabel lblfun_img_1,lblfun_img_2, lblfun_img_3,lblfun_img_4, lblfun_img_5;
	private JLabel lblcond_img_1, lblcond_img_2,lblcond_img_3, lblcond_img_4, lblcond_img_5;
	
	private JPanel panel_detail_img;
	private JLabel lbl_detail_img1, lbl_detail_img2, lbl_detail_img3, lbl_detail_img4, lbl_detail_img5, lbl_detail_img6, lbl_detail_img7,
	lbl_detail_img8, lbl_detail_img9, lbl_detail_img10, lbl_detail_img11, lbl_detail_img12;
	private JLabel lbl_start;
	private JLabel lbl_end;
	private JProgressBar progressBar;

	public Course_GUI(UserVO uvo, int selectRow) {
		initialize(uvo, selectRow);
		frame.setVisible(true);
	}

	private void initialize(UserVO uvo, int selectRow) {
		frame = new JFrame();
		frame.setBounds(100, 100, 565, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		DAO dao = new DAO();
		int no = selectRow + 1;

		ArrayList<CourseVO> cvo = dao.courseInfo(no);

		lblCourse = new JLabel(cvo.get(0).getCourse_no() + " " + cvo.get(0).getCourse_name());
		lblCourse.setFont(new Font("monospaced", Font.BOLD, 30));
		lblCourse.setHorizontalAlignment(SwingConstants.CENTER);
		lblCourse.setBounds(10, 10, 530, 45);
		frame.getContentPane().add(lblCourse);

		lblCourseThm = new JLabel(cvo.get(0).getCourse_thm());
		lblCourseThm.setHorizontalAlignment(SwingConstants.CENTER);
		lblCourseThm.setFont(new Font("Monospaced", Font.BOLD, 24));
		lblCourseThm.setBounds(10, 55, 530, 45);
		frame.getContentPane().add(lblCourseThm);

		String intro = cvo.get(0).getCourse_intro();

		lblDurTime = new JLabel(cvo.get(0).getDuration_time());
		lblDurTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblDurTime.setFont(new Font("Monospaced", Font.BOLD, 15));
		lblDurTime.setBounds(220, 265, 80, 30);
		frame.getContentPane().add(lblDurTime);

		lblCourseLen = new JLabel(cvo.get(0).getCourse_len());
		lblCourseLen.setHorizontalAlignment(SwingConstants.CENTER);
		lblCourseLen.setFont(new Font("Monospaced", Font.BOLD, 15));
		lblCourseLen.setBounds(220, 290, 80, 30);
		frame.getContentPane().add(lblCourseLen);

		lblother1 = new JLabel(cvo.get(0).getCourse_other1());
		lblother1.setHorizontalAlignment(SwingConstants.CENTER);
		lblother1.setBounds(10, 470, 170, 40);
		frame.getContentPane().add(lblother1);

		lblother2 = new JLabel(cvo.get(0).getCourse_other2());
		lblother2.setHorizontalAlignment(SwingConstants.CENTER);
		lblother2.setBounds(185, 470, 170, 40);
		frame.getContentPane().add(lblother2);

		lblother3 = new JLabel(cvo.get(0).getCourse_other3());
		lblother3.setHorizontalAlignment(SwingConstants.CENTER);
		lblother3.setBounds(370, 470, 170, 40);
		frame.getContentPane().add(lblother3);

		btnreservation = new JButton("\uC608\uC57D");
		btnreservation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Reservation rv = new Reservation(uvo, cvo);
				frame.dispose();
			}

		});
		btnreservation.setBounds(321, 628, 97, 23);
		frame.getContentPane().add(btnreservation);

		btncourse = new JButton("\uB3CC\uC544\uAC00\uAE30");
		btncourse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CoursePage cp = new CoursePage(uvo);
				frame.dispose();
			}
		});
		btncourse.setBounds(430, 628, 97, 23);
		frame.getContentPane().add(btncourse);

		scrollPane_info = new JScrollPane();
		scrollPane_info.setBounds(10, 150, 530, 115);
		frame.getContentPane().add(scrollPane_info);
		
		lblCourseIntro = new JLabel();
		lblCourseIntro.setFont(new Font("Monospaced", Font.PLAIN, 14));
		scrollPane_info.setViewportView(lblCourseIntro);
		lblCourseIntro.setText("<html><p style=\"width:350px\">" + intro + "</p></html>");

		scrollPane_img = new JScrollPane();
		scrollPane_img.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane_img.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		scrollPane_img.setBounds(10, 320, 530, 150);
		frame.getContentPane().add(scrollPane_img);
		
		panel_detail_img = new JPanel();
		panel_detail_img.getAutoscrolls();
		// ***** 사이즈 유동적으로 변경 가능 설정 *****
		panel_detail_img.setPreferredSize(new Dimension(1628, 150));
		scrollPane_img.setViewportView(panel_detail_img);
		panel_detail_img.setLayout(null);

		lblDiff = new JLabel("난이도");
		lblDiff.setFont(new Font("Monospaced", Font.BOLD, 12));
		lblDiff.setHorizontalAlignment(SwingConstants.CENTER);
		lblDiff.setBounds(50, 90, 70, 25);
		frame.getContentPane().add(lblDiff);

		lblFun = new JLabel("\uC7AC\uBBF8");
		lblFun.setHorizontalAlignment(SwingConstants.CENTER);
		lblFun.setFont(new Font("Monospaced", Font.BOLD, 12));
		lblFun.setBounds(230, 90, 70, 25);
		frame.getContentPane().add(lblFun);

		lblCond = new JLabel("\uCEE8\uB514\uC158");
		lblCond.setHorizontalAlignment(SwingConstants.CENTER);
		lblCond.setFont(new Font("Monospaced", Font.BOLD, 12));
		lblCond.setBounds(430, 90, 70, 25);
		frame.getContentPane().add(lblCond);
		
		lbl_start = new JLabel(cvo.get(0).getCourse_start());
		lbl_start.setFont(new Font("Monospaced", Font.BOLD | Font.ITALIC, 15));
		lbl_start.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_start.setBounds(20, 280, 100, 30);
		frame.getContentPane().add(lbl_start);
		
		lbl_end = new JLabel(cvo.get(0).getCourse_end());
		lbl_end.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_end.setFont(new Font("Monospaced", Font.BOLD | Font.ITALIC, 15));
		lbl_end.setBounds(430, 280, 100, 30);
		frame.getContentPane().add(lbl_end);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(130, 290, 290, 5);
		frame.getContentPane().add(progressBar);
		
		int diff = cvo.get(0).getRating_diff();
		int fun = cvo.get(0).getRating_fun();
		int cond = cvo.get(0).getRating_cond();

		// star img
		ImageIcon ic_star_full = new ImageIcon("data/img/gui/star.png");
		ImageIcon ic_star_empty = new ImageIcon("data/img/gui/star_empty.png");

		// 크기 변경
		Image img_star_full = ic_star_full.getImage();
		Image img_star_full2 = img_star_full.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		ImageIcon ic_star_full2 = new ImageIcon(img_star_full2);
		
		Image img_star_empty = ic_star_empty.getImage();
		Image img_star_empty2 = img_star_empty.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		ImageIcon ic_star_empty2 = new ImageIcon(img_star_empty2);
		// 
		
		// star Ratting
		if(diff == 1) {
			lbldiff_img_1 = new JLabel(ic_star_full2);
			lbldiff_img_2 = new JLabel(ic_star_empty2);
			lbldiff_img_3 = new JLabel(ic_star_empty2);
			lbldiff_img_4 = new JLabel(ic_star_empty2);
			lbldiff_img_5 = new JLabel(ic_star_empty2);
			
			lbldiff_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_1.setBounds(10, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_1);
			
			lbldiff_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_2.setBounds(40, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_2);
			
			lbldiff_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_3.setBounds(70, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_3);
			
			lbldiff_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_4.setBounds(100, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_4);
			
			lbldiff_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_5.setBounds(130, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_5);
		}else if(diff ==2) {
			lbldiff_img_1 = new JLabel(ic_star_full2);
			lbldiff_img_2 = new JLabel(ic_star_full2);
			lbldiff_img_3 = new JLabel(ic_star_empty2);
			lbldiff_img_4 = new JLabel(ic_star_empty2);
			lbldiff_img_5 = new JLabel(ic_star_empty2);
			
			lbldiff_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_1.setBounds(10, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_1);
			
			lbldiff_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_2.setBounds(40, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_2);
			
			lbldiff_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_3.setBounds(70, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_3);
			
			lbldiff_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_4.setBounds(100, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_4);
			
			lbldiff_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_5.setBounds(130, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_5);
		}
		if(diff ==3) {
			lbldiff_img_1 = new JLabel(ic_star_full2);
			lbldiff_img_2 = new JLabel(ic_star_full2);
			lbldiff_img_3 = new JLabel(ic_star_full2);
			lbldiff_img_4 = new JLabel(ic_star_empty2);
			lbldiff_img_5 = new JLabel(ic_star_empty2);
			
			lbldiff_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_1.setBounds(10, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_1);
			
			lbldiff_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_2.setBounds(40, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_2);
			
			lbldiff_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_3.setBounds(70, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_3);
			
			lbldiff_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_4.setBounds(100, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_4);
			
			lbldiff_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_5.setBounds(130, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_5);
		}else if(diff ==4) {
			
			lbldiff_img_1 = new JLabel(ic_star_full2);
			lbldiff_img_2 = new JLabel(ic_star_full2);
			lbldiff_img_3 = new JLabel(ic_star_full2);
			lbldiff_img_4 = new JLabel(ic_star_full2);
			lbldiff_img_5 = new JLabel(ic_star_empty2);
			
			lbldiff_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_1.setBounds(10, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_1);
			
			lbldiff_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_2.setBounds(40, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_2);
			
			lbldiff_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_3.setBounds(70, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_3);
			
			lbldiff_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_4.setBounds(100, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_4);
			
			lbldiff_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_5.setBounds(130, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_5);
		}else if(diff==5) {
			lbldiff_img_1 = new JLabel(ic_star_full2);
			lbldiff_img_2 = new JLabel(ic_star_full2);
			lbldiff_img_3 = new JLabel(ic_star_full2);
			lbldiff_img_4 = new JLabel(ic_star_full2);
			lbldiff_img_5 = new JLabel(ic_star_full2);
			
			lbldiff_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_1.setBounds(10, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_1);
			
			lbldiff_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_2.setBounds(40, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_2);
			
			lbldiff_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_3.setBounds(70, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_3);
			
			lbldiff_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_4.setBounds(100, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_4);
			
			lbldiff_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lbldiff_img_5.setBounds(130, 110, 30, 30);
			frame.getContentPane().add(lbldiff_img_5);
		}
		
		if(fun ==1) {
			lblfun_img_1 = new JLabel(ic_star_full2);
			lblfun_img_2 = new JLabel(ic_star_empty2);
			lblfun_img_3 = new JLabel(ic_star_empty2);
			lblfun_img_4 = new JLabel(ic_star_empty2);
			lblfun_img_5 = new JLabel(ic_star_empty2);
			
			lblfun_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_1.setSize(30,30);
			lblfun_img_1.setLocation(200,110);
			frame.getContentPane().add(lblfun_img_1);
			
			lblfun_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_2.setSize(30,30);
			lblfun_img_2.setLocation(230,110);
			frame.getContentPane().add(lblfun_img_2);
			
			lblfun_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_3.setSize(30,30);
			lblfun_img_3.setLocation(260,110);
			frame.getContentPane().add(lblfun_img_3);
			
			lblfun_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_4.setSize(30,30);
			lblfun_img_4.setLocation(290,110);
			frame.getContentPane().add(lblfun_img_4);
			
			lblfun_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_5.setSize(30,30);
			lblfun_img_5.setLocation(320,110);
			frame.getContentPane().add(lblfun_img_5);
		}else if(fun ==2) {
			lblfun_img_1 = new JLabel(ic_star_full2);
			lblfun_img_2 = new JLabel(ic_star_full2);
			lblfun_img_3 = new JLabel(ic_star_empty2);
			lblfun_img_4 = new JLabel(ic_star_empty2);
			lblfun_img_5 = new JLabel(ic_star_empty2);
			
			lblfun_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_1.setSize(30,30);
			lblfun_img_1.setLocation(200,110);
			frame.getContentPane().add(lblfun_img_1);
			
			lblfun_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_2.setSize(30,30);
			lblfun_img_2.setLocation(230,110);
			frame.getContentPane().add(lblfun_img_2);
			
			lblfun_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_3.setSize(30,30);
			lblfun_img_3.setLocation(260,110);
			frame.getContentPane().add(lblfun_img_3);
			
			lblfun_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_4.setSize(30,30);
			lblfun_img_4.setLocation(290,110);
			frame.getContentPane().add(lblfun_img_4);
			
			lblfun_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_5.setSize(30,30);
			lblfun_img_5.setLocation(320,110);
			frame.getContentPane().add(lblfun_img_5);
		}else if(fun ==3) {
			lblfun_img_1 = new JLabel(ic_star_full2);
			lblfun_img_2 = new JLabel(ic_star_full2);
			lblfun_img_3 = new JLabel(ic_star_full2);
			lblfun_img_4 = new JLabel(ic_star_empty2);
			lblfun_img_5 = new JLabel(ic_star_empty2);
			
			lblfun_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_1.setSize(30,30);
			lblfun_img_1.setLocation(200,110);
			frame.getContentPane().add(lblfun_img_1);
			
			lblfun_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_2.setSize(30,30);
			lblfun_img_2.setLocation(230,110);
			frame.getContentPane().add(lblfun_img_2);
			
			lblfun_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_3.setSize(30,30);
			lblfun_img_3.setLocation(260,110);
			frame.getContentPane().add(lblfun_img_3);
			
			lblfun_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_4.setSize(30,30);
			lblfun_img_4.setLocation(290,110);
			frame.getContentPane().add(lblfun_img_4);
			
			lblfun_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_5.setSize(30,30);
			lblfun_img_5.setLocation(320,110);
			frame.getContentPane().add(lblfun_img_5);
			
		}else if(fun == 4) {
			
			lblfun_img_1 = new JLabel(ic_star_full2);
			lblfun_img_2 = new JLabel(ic_star_full2);
			lblfun_img_3 = new JLabel(ic_star_full2);
			lblfun_img_4 = new JLabel(ic_star_full2);
			lblfun_img_5 = new JLabel(ic_star_empty2);
			
			lblfun_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_1.setSize(30,30);
			lblfun_img_1.setLocation(200,110);
			frame.getContentPane().add(lblfun_img_1);
			
			lblfun_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_2.setSize(30,30);
			lblfun_img_2.setLocation(230,110);
			frame.getContentPane().add(lblfun_img_2);
			
			lblfun_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_3.setSize(30,30);
			lblfun_img_3.setLocation(260,110);
			frame.getContentPane().add(lblfun_img_3);
			
			lblfun_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_4.setSize(30,30);
			lblfun_img_4.setLocation(290,110);
			frame.getContentPane().add(lblfun_img_4);
			
			lblfun_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_5.setSize(30,30);
			lblfun_img_5.setLocation(320,110);
			frame.getContentPane().add(lblfun_img_5);
		}else if(fun ==5) {
			
			lblfun_img_1 = new JLabel(ic_star_full2);
			lblfun_img_2 = new JLabel(ic_star_full2);
			lblfun_img_3 = new JLabel(ic_star_full2);
			lblfun_img_4 = new JLabel(ic_star_full2);
			lblfun_img_5 = new JLabel(ic_star_full2);
			
			lblfun_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_1.setSize(30,30);
			lblfun_img_1.setLocation(200,110);
			frame.getContentPane().add(lblfun_img_1);
			
			lblfun_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_2.setSize(30,30);
			lblfun_img_2.setLocation(230,110);
			frame.getContentPane().add(lblfun_img_2);
			
			lblfun_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_3.setSize(30,30);
			lblfun_img_3.setLocation(260,110);
			frame.getContentPane().add(lblfun_img_3);
			
			lblfun_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_4.setSize(30,30);
			lblfun_img_4.setLocation(290,110);
			frame.getContentPane().add(lblfun_img_4);
			
			lblfun_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblfun_img_5.setSize(30,30);
			lblfun_img_5.setLocation(320,110);
			frame.getContentPane().add(lblfun_img_5);
		}
		
		if(cond ==1) {
			lblcond_img_1 = new JLabel(ic_star_full2);
			lblcond_img_2 = new JLabel(ic_star_empty2);
			lblcond_img_3 = new JLabel(ic_star_empty2);
			lblcond_img_4 = new JLabel(ic_star_empty2);
			lblcond_img_5 = new JLabel(ic_star_empty2);
			
			lblcond_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_1.setSize(30,30);
			lblcond_img_1.setLocation(380,110);
			frame.getContentPane().add(lblcond_img_1);
			
			lblcond_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_2.setBounds(410, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_2);
			
			lblcond_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_3.setBounds(440, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_3);
			
			lblcond_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_4.setBounds(470, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_4);
			
			lblcond_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_5.setBounds(500, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_5);
		}else if(cond == 2) {
			
			lblcond_img_1 = new JLabel(ic_star_full2);
			lblcond_img_2 = new JLabel(ic_star_full2);
			lblcond_img_3 = new JLabel(ic_star_empty2);
			lblcond_img_4 = new JLabel(ic_star_empty2);
			lblcond_img_5 = new JLabel(ic_star_empty2);
			
			lblcond_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_1.setSize(30,30);
			lblcond_img_1.setLocation(380,110);
			frame.getContentPane().add(lblcond_img_1);
			
			lblcond_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_2.setBounds(410, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_2);
			
			lblcond_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_3.setBounds(440, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_3);
			
			lblcond_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_4.setBounds(470, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_4);
			
			lblcond_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_5.setBounds(500, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_5);
		}else if(cond == 3) {
			lblcond_img_1 = new JLabel(ic_star_full2);
			lblcond_img_2 = new JLabel(ic_star_full2);
			lblcond_img_3 = new JLabel(ic_star_full2);
			lblcond_img_4 = new JLabel(ic_star_empty2);
			lblcond_img_5 = new JLabel(ic_star_empty2);
			
			lblcond_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_1.setSize(30,30);
			lblcond_img_1.setLocation(380,110);
			frame.getContentPane().add(lblcond_img_1);
			
			lblcond_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_2.setBounds(410, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_2);
			
			lblcond_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_3.setBounds(440, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_3);
			
			lblcond_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_4.setBounds(470, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_4);
			
			lblcond_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_5.setBounds(500, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_5);
		}else if(cond == 4) {
			lblcond_img_1 = new JLabel(ic_star_full2);
			lblcond_img_2 = new JLabel(ic_star_full2);
			lblcond_img_3 = new JLabel(ic_star_full2);
			lblcond_img_4 = new JLabel(ic_star_full2);
			lblcond_img_5 = new JLabel(ic_star_empty2);
			
			lblcond_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_1.setSize(30,30);
			lblcond_img_1.setLocation(380,110);
			frame.getContentPane().add(lblcond_img_1);
			
			lblcond_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_2.setBounds(410, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_2);
			
			lblcond_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_3.setBounds(440, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_3);
			
			lblcond_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_4.setBounds(470, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_4);
			
			lblcond_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_5.setBounds(500, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_5);
		}else if(cond == 5) {
			lblcond_img_1 = new JLabel(ic_star_full2);
			lblcond_img_2 = new JLabel(ic_star_full2);
			lblcond_img_3 = new JLabel(ic_star_full2);
			lblcond_img_4 = new JLabel(ic_star_full2);
			lblcond_img_5 = new JLabel(ic_star_full2);
			
			lblcond_img_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_1.setSize(30,30);
			lblcond_img_1.setLocation(380,110);
			frame.getContentPane().add(lblcond_img_1);
			
			lblcond_img_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_2.setBounds(410, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_2);
			
			lblcond_img_3.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_3.setBounds(440, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_3);
			
			lblcond_img_4.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_4.setBounds(470, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_4);
			
			lblcond_img_5.setHorizontalAlignment(SwingConstants.CENTER);
			lblcond_img_5.setBounds(500, 110, 30, 30);
			frame.getContentPane().add(lblcond_img_5);
		}
		
		// detail_img
		String str_img_detail_root = new File("data/img/crolling/course_detail").getAbsolutePath();
		for(int i = 1; i<=12; i++) {
			ImageIcon ic_detail_img = new ImageIcon(str_img_detail_root+"/"+selectRow+"/"+i+".jpg");

			// 크기 변경
			Image img_detail = ic_detail_img.getImage();
			Image img_detail2 = img_detail.getScaledInstance(148, 148, Image.SCALE_SMOOTH);
			ImageIcon ic_detail_img2 = new ImageIcon(img_detail2);
			
			// 사진 넣어주기
			if(i == 1) {
				lbl_detail_img1 = new JLabel(ic_detail_img2);
				lbl_detail_img1.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img1.setBounds(0, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img1);
			}else if(i == 2) {
				lbl_detail_img2 = new JLabel(ic_detail_img2);
				lbl_detail_img2.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img2.setBounds(148, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img2);
			}else if(i == 3) {
				lbl_detail_img3 = new JLabel(ic_detail_img2);
				lbl_detail_img3.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img3.setBounds(296, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img3);
			}else if(i == 4) {
				lbl_detail_img4 = new JLabel(ic_detail_img2);
				lbl_detail_img4.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img4.setBounds(444, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img4);
			}else if(i == 5) {
				lbl_detail_img5 = new JLabel(ic_detail_img2);
				lbl_detail_img5.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img5.setBounds(592, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img5);
			}else if(i == 6) {
				lbl_detail_img6 = new JLabel(ic_detail_img2);
				lbl_detail_img6.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img6.setBounds(740, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img6);
			}else if(i == 7) {
				lbl_detail_img7 = new JLabel(ic_detail_img2);
				lbl_detail_img7.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img7.setBounds(888, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img7);
			}else if(i == 8) {
				lbl_detail_img8 = new JLabel(ic_detail_img2);
				lbl_detail_img8.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img8.setBounds(1036, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img8);
			}else if(i == 9) {
				lbl_detail_img9 = new JLabel(ic_detail_img2);
				lbl_detail_img9.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img9.setBounds(1184, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img9);
			}else if(i == 10) {
				lbl_detail_img10 = new JLabel(ic_detail_img2);
				lbl_detail_img10.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img10.setBounds(1332, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img10);
			}else if(i == 11) {
				lbl_detail_img11 = new JLabel(ic_detail_img2);
				lbl_detail_img11.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img11.setBounds(1480, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img11);
			}else if(i == 12) {
				lbl_detail_img12 = new JLabel(ic_detail_img2);
				lbl_detail_img12.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_detail_img12.setBounds(1628, 0, 148, 148);
				panel_detail_img.add(lbl_detail_img12);
			}
			
			
		}
		
		// other_img
		String str_img_etc_root = new File("data/img/crolling/course_etc").getAbsolutePath();
		for(int i = 1; i<=3; i++) {
			ImageIcon ic_etc_img = new ImageIcon(str_img_etc_root+"/"+selectRow+"/"+i+".jpg");

			// 크기 변경
			Image img_etc = ic_etc_img.getImage();
			Image img_etc2 = img_etc.getScaledInstance(148, 148, Image.SCALE_SMOOTH);
			ImageIcon ic_etc_img2 = new ImageIcon(img_etc2);
			
			// 사진 넣어주기
			if(i == 1) {
				lbl_etc_img1 = new JLabel(ic_etc_img2);
				lbl_etc_img1.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_etc_img1.setBounds(35, 510, 120, 110);
				frame.getContentPane().add(lbl_etc_img1);
			}else if(i == 2) {
				lbl_etc_img2 = new JLabel(ic_etc_img2);
				lbl_etc_img2.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_etc_img2.setBounds(209, 510, 120, 110);
				frame.getContentPane().add(lbl_etc_img2);
			}else if(i == 3) {
				lbl_etc_img3 = new JLabel(ic_etc_img2);
				lbl_etc_img3.setHorizontalAlignment(SwingConstants.CENTER);
				lbl_etc_img3.setBounds(395, 510, 120, 110);
				frame.getContentPane().add(lbl_etc_img3);
			}
			
			
		}


	}
}