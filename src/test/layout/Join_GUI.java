package test.layout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import test.DAO;
import test.UserVO;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Join_GUI {

	private JFrame frame;
	private JTextField txt_id;
	private JTextField txt_pw;
	private JTextField txt_name;
	private JTextField txt_lisencs;
	private UserVO uservo = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Join_GUI window = new Join_GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Join_GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 380);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbl_title = new JLabel("\uD68C\uC6D0\uAC00\uC785");
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_title.setBounds(10, 10, 360, 50);
		frame.getContentPane().add(lbl_title);
		
		JLabel lbl_id = new JLabel("ID");
		lbl_id.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_id.setBounds(30, 70, 90, 35);
		frame.getContentPane().add(lbl_id);
		
		JLabel lbl_pw = new JLabel("PASSWORD");
		lbl_pw.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_pw.setBounds(30, 115, 90, 35);
		frame.getContentPane().add(lbl_pw);
		
		JLabel lbl_name = new JLabel("\uC774\uB984");
		lbl_name.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_name.setBounds(30, 160, 90, 35);
		frame.getContentPane().add(lbl_name);
		
		txt_id = new JTextField();
		txt_id.setBounds(140, 70, 200, 35);
		frame.getContentPane().add(txt_id);
		txt_id.setColumns(10);
		
		txt_pw = new JTextField();
		txt_pw.setColumns(10);
		txt_pw.setBounds(140, 115, 200, 35);
		frame.getContentPane().add(txt_pw);
		
		txt_name = new JTextField();
		txt_name.setColumns(10);
		txt_name.setBounds(140, 160, 200, 35);
		frame.getContentPane().add(txt_name);
		
		JLabel lbl_license = new JLabel("\uB77C\uC774\uC13C\uC2A4 \uCF54\uB4DC");
		lbl_license.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_license.setBounds(30, 205, 90, 35);
		frame.getContentPane().add(lbl_license);
		
		txt_lisencs = new JTextField();
		txt_lisencs.setColumns(10);
		txt_lisencs.setBounds(140, 205, 200, 35);
		frame.getContentPane().add(txt_lisencs);
		
		JButton btn_add = new JButton("\uD68C\uC6D0\uAC00\uC785");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String id = txt_id.getText();
				String pw = txt_pw.getText();
				String name = txt_name.getText();
				String lisence = txt_lisencs.getText();
				
				if( id != null && pw != null && name != null && lisence != null) {
					uservo = new UserVO(id, pw, name, lisence);
					DAO dao = new DAO();
					int check = dao.check_userInfoID(uservo);
					
					if(check > 0) {
						JOptionPane.showMessageDialog(lbl_license, "이미 등록된 유저입니다.\n아이디를 확인해주세요");
						
					}else {
						int check2 = dao.check_userInfoLicense(uservo);
						if(check2 > 0) {
							JOptionPane.showMessageDialog(lbl_license, "이미 등록된 유저입니다.\n라이센스를 확인해주세요");
						}else {
							int result = dao.insert_userInfo(uservo);
							
							if(result > 0) {
								JOptionPane.showMessageDialog(lbl_license, id+"님, 가입을 축하드립니다");
								
								txt_id.setText(null);
								txt_pw.setText(null);
								txt_name.setText(null);
								txt_lisencs.setText(null);
								
								frame.dispose();
								
								Login_GUI login = new Login_GUI();
								login.main(null);
							}else {
								JOptionPane.showMessageDialog(lbl_license, "등록 오류. \n 다시 시도해주세요");
							}

						}
						
							
						

					}
				}
			}
		});
		btn_add.setBounds(150, 270, 190, 35);
		frame.getContentPane().add(btn_add);
		

	}
}
