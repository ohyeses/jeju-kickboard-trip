package garbage;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MapView extends JFrame {
	
	private JTextField textField = new JTextField(30);
	private JPanel panel = new JPanel();
	
	private GoogleAPI googleAPI = new GoogleAPI();
	private String location = "����";
	private String xPoint="";
	private String yPoint="";
	private JLabel googleMap;

	public MapView() {
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setTitle("Google Maps");
		setVisible(true);
		
		panel.add(textField);
		
		
		googleAPI.downloadMap(location);
		googleMap = new JLabel(googleAPI.getMap(location));
		googleAPI.fileDelete(location);
		
		add(BorderLayout.NORTH, panel);
		add(BorderLayout.SOUTH, googleMap);
		pack();
	}
	
	public void MapMarker() {
		
	}

}
