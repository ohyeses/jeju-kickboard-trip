package ProjectMain;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 550, 850);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbl_title = new JLabel("Title");
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_title.setFont(new Font("����", Font.PLAIN, 32));
		lbl_title.setBounds(190, 55, 170, 40);
		frame.getContentPane().add(lbl_title);
		
		JLabel lblNewLabel = new JLabel("LOGO");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(150, 50, 50, 50);
		frame.getContentPane().add(lblNewLabel);
		
		TitledBorder oneTb = new TitledBorder(new LineBorder(Color.black));
		JPanel panel = new JPanel();
		panel.setBounds(90, 120, 360, 200);
		panel.setBorder(oneTb);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lbl_id = new JLabel("ID : ");
		lbl_id.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_id.setBounds(12, 55, 60, 20);
		panel.add(lbl_id);
		
		JTextPane txtp_id = new JTextPane();
		txtp_id.setBounds(61, 55, 130, 20);
		panel.add(txtp_id);
		
		JTextPane txtp_pw = new JTextPane();
		txtp_pw.setBounds(61, 104, 130, 20);
		panel.add(txtp_pw);
		
		JLabel lbl_pw = new JLabel("PW : ");
		lbl_pw.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_pw.setBounds(12, 104, 60, 20);
		panel.add(lbl_pw);
		
		JButton btn_login = new JButton("Login");
		btn_login.setBounds(248, 50, 100, 30);
		panel.add(btn_login);
		
		JButton btn_join = new JButton("Join");
		btn_join.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_join.setBounds(248, 103, 100, 30);
		panel.add(btn_join);
	}
}
