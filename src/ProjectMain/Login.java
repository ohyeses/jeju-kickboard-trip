package ProjectMain;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class Login {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 550, 850);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lbl_title = new JLabel("Title");
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_title.setFont(new Font("����", Font.PLAIN, 32));
		lbl_title.setBounds(190, 55, 170, 40);
		frame.getContentPane().add(lbl_title);
		
		JLabel lblNewLabel = new JLabel("LOGO");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(150, 50, 50, 50);
		frame.getContentPane().add(lblNewLabel);
		
		TitledBorder oneTb = new TitledBorder(new LineBorder(Color.black));
		JPanel panel = new JPanel();
		panel.setBounds(90, 120, 360, 600);
		panel.setBorder(oneTb);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton btn_regi = new JButton("Register");
		btn_regi.setBounds(350, 730, 100, 30);
		frame.getContentPane().add(btn_regi);
	}

}
