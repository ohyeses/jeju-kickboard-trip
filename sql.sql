create table course_info(
course_no number(10) not null,
course_name varchar2(50) not null,
course_thm varchar2(100) not null,
course_intro varchar2(2000) not null,
course_start varchar2(20) not null,
course_end varchar2(20) not null,
duration_time varchar2(20) not null,
course_len varchar2(20) not null,
rating_diff number(10) not null,
rating_fun number(10) not null,
rating_cond number(10) not null,
pic_path varchar2(100) not null,
course_other1 varchar2(100) not null,
course_other2 varchar2(100) not null,
course_other3 varchar2(100) not null,
constraint courinfo_courseid_pk primary key(course_no));


create table user_info(
user_id varchar(20), 
user_pw varchar(20) not null,
user_name varchar(20) not null,
user_license varchar(30) not null,
constraint user_id_pk primary key(user_id),
constraint user_license_uk  unique(user_license));


create table reservation (
user_id varchar(20),
course_no number(10), 
reservation_date date,
constraint reservation_idcoursedate_pk primary key(
user_id,course_no,reservation_date),
constraint reservation_userid_fk foreign key (user_id)
references user_info(user_id),
constraint reservation_courseno_fk foreign key(course_no)
references course_info(course_no));


create table review(
review_no number(20) not null,
course_no number(10) not null,
user_id varchar(20) not null,
urating_diff number(10),
urating_fun number(10),
urating_cond number(10),
review varchar2(1000),
constraint review_reviewno_pk primary key(review_no), 
constraint review_userno_fk foreign key(user_id) references user_info(user_id),
constraint review_couerseno_fk foreign key(course_no) references course_info(course_no));