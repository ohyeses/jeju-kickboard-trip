package test;

public class CourseVO {
	private int course_no;
	private	String course_name;
	private String course_thm;
	private String course_intro;
	private String course_start;
	private String course_end;
	private String duration_time;
	private String course_len;
	private int rating_diff;
	private int rating_fun;
	private int rating_cond;
	private String pic_path;
	private String course_other1;
	private String course_other2;
	private String course_other3;
	
	public int getCourse_no() {
		return course_no;
	}
	public void setCourse_no(int course_no) {
		this.course_no = course_no;
	}
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public String getCourse_thm() {
		return course_thm;
	}
	public void setCourse_thm(String course_thm) {
		this.course_thm = course_thm;
	}
	public String getCourse_intro() {
		return course_intro;
	}
	public void setCourse_intro(String course_intro) {
		this.course_intro = course_intro;
	}
	public String getCourse_start() {
		return course_start;
	}
	public void setCourse_start(String course_start) {
		this.course_start = course_start;
	}
	public String getCourse_end() {
		return course_end;
	}
	public void setCourse_end(String course_end) {
		this.course_end = course_end;
	}
	public String getDuration_time() {
		return duration_time;
	}
	public void setDuration_time(String duration_time) {
		this.duration_time = duration_time;
	}
	public String getCourse_len() {
		return course_len;
	}
	public void setCourse_len(String course_len) {
		this.course_len = course_len;
	}
	public int getRating_diff() {
		return rating_diff;
	}
	public void setRating_diff(int rating_diff) {
		this.rating_diff = rating_diff;
	}
	public int getRating_fun() {
		return rating_fun;
	}
	public void setRating_fun(int rating_fun) {
		this.rating_fun = rating_fun;
	}
	public int getRating_cond() {
		return rating_cond;
	}
	public void setRating_cond(int rating_cond) {
		this.rating_cond = rating_cond;
	}
	public String getPic_path() {
		return pic_path;
	}
	public void setPic_path(String pic_path) {
		this.pic_path = pic_path;
	}
	public String getCourse_other1() {
		return course_other1;
	}
	public void setCourse_other1(String course_other1) {
		this.course_other1 = course_other1;
	}
	public String getCourse_other2() {
		return course_other2;
	}
	public void setCourse_other2(String course_other2) {
		this.course_other2 = course_other2;
	}
	public String getCourse_other3() {
		return course_other3;
	}
	public void setCourse_other3(String course_other3) {
		this.course_other3 = course_other3;
	}
	public CourseVO(int course_no, String course_name, String course_thm, String course_intro, String course_start,
			String course_end, String duration_time, String course_len, int rating_diff, int rating_fun,
			int rating_cond,String pic_path, String course_other1, String course_other2, String course_other3) {

		this.course_no = course_no;
		this.course_name = course_name;
		this.course_thm = course_thm;
		this.course_intro = course_intro;
		this.course_start = course_start;
		this.course_end = course_end;
		this.duration_time = duration_time;
		this.course_len = course_len;
		this.rating_diff = rating_diff;
		this.rating_fun = rating_fun;
		this.rating_cond = rating_cond;
		this.pic_path=pic_path;
		this.course_other1 = course_other1;
		this.course_other2 = course_other2;
		this.course_other3 = course_other3;
	}
	public CourseVO(int course_no2, String course_name2, int rating_diff2, int rating_fun2, int rating_cond2) {
		// TODO Auto-generated constructor stub
		this.course_no = course_no2;
		this.course_name = course_name2;
		this.rating_diff = rating_diff2;
		this.rating_fun = rating_fun2;
		this.rating_cond = rating_cond2;
	}
	
	
	
}
